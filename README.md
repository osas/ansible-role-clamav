# Ansible role for ClamAV installation

[ClamAV](https://www.clamav.net/) is an open source antivirus engine for detecting trojans, viruses, malware & other malicious threats.

This roles installs and configure Clamd, the daemonized version, to run on systems who need regular checks (mail servers for example). Freshclam, used to update the virus database regularly is also installed.

The configuration also enables access via the UNIX socket; by default any user can request a check but this can be changed with `restrict_unix_socket`. Additionally you can enable the network socket too with `with_tcp`; the firewall configuration is left to your care.

The daemon is configured to run as root to be able to check any file not protected by SELinux on the filesystem.

Currently this role only handles Red Hat systems (but contributions are welcome).

Example:
```
- hosts: mx1.example.com
  tasks:
    - include_role:
        name: clamav
      vars:
        max_threads: 50
```

## Variables

This role does not expose a lot of parameters as it tries to stay simple and in most cases this is totaly sufficient.

- *clamd_instance*: the default instance is named 'scan' but custom instances can be setup.
- *restrict_unix_socket*: when False, the default, any user can request a check on the UNIX socket; if True only members of the `virusgroup` group are allowed
                          on Red Hat systems the directory holding the socket has restritive permissions enforced for the default instance so this paremeter will not work (but will work for custom instances).
- *with_tcp*: enable the network socket if True (defaults to False)
- *tcp_port*: TCP port to listen to if `with_tcp` is True (defaults to 3310)
- *tcp_listen*: address to listen to if `with_tcp` is True (defaults to 127.0.0.1)
- *max_threads*: maximum threads Clamd is allowed to spawn (defaults to 10)

